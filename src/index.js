import style from './styles/styles.scss'
import React from 'react'
import ReactDOM from 'react-dom'
import Home from './components/Home'
import BasicRouting from './components/BasicRouting'
import {
  BrowserRouter,
  Switch,
  Route,
  NavLink,
  Link,
  HashRouter,
} from 'react-router-dom'

if (process.env.NODE_ENV !== 'production') {
  console.log('Looks like we are in development mode!')
}

var element = document.createElement('app');

ReactDOM.render(
  <HashRouter>
    <div>
      <ul>
        <li><NavLink to="/" activeClassName="active">Home</NavLink></li>
        <li><NavLink to="basic-routing" activeClassName="active">Basic Routing</NavLink></li>
      </ul>

      <Switch>
        <Route path="/" component={Home} exact={true}/>
        <Route path="/basic-routing" component={BasicRouting}/>
      </Switch>
    </div>
  </HashRouter>,
  document.getElementById('app'),
)